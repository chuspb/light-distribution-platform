#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
# Light Distribution Platform
# ===========================
#
# Copyright (C) 2005-2014 All rights reserved by
#   Francisco José Moreno Llorca <packo@assamita.net>
#   Francisco Jesús González Mata <chuspb@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import smtplib,ConfigParser,httplib

def getConfig(conf_path,sections):
    """
        read configurations from a config.ini
        with a section list
        and return as dictionary
    """
    
    return_dict = {}
    parser = ConfigParser.ConfigParser()
    try:
        parser.readfp(open(conf_path))
    except:
        print("Leyendo archivo de configuraciones")
    
    try:
        for section in sections:
            for param in parser.items(section): 
                try:
                    return_dict[param[0]] = param[1]
                except:
                    print("Error getting: "+section+":"+param)
    except Exception,e:
        print("Error getting information from conf.ini: "+ str(e))
        
    return return_dict


def send_email(msg,fromaddress,toaddress,host):
    """
        send a message
    """
    server = smtplib.SMTP(host)
    server.set_debuglevel(1)
    server.sendmail(fromaddress, toaddress, msg)
    server.quit()
    
def downloadFile(host,port,url,dstpath):
    """
        download a file from host + url and save into dstpath
        if there is any error, return False, else return True
    """

    try:
        conexion = httplib.HTTPConnection(host,port)
        conexion.request('GET',url)
        dst_response = conexion.getresponse()
        if dst_response.status != 200:
            return False
    except Exception,e:
        print(str(e))
        return False
        
    #save to disk
    try:
        file = open(dstpath,'wb')
        mensaje = dst_response.read()
        file.write(mensaje)
        file.close()
        return True
    except Exception,e:
        print(str(e))
        return False

def check_couchdb_up(host,port):
    """
        check if the local couch db is running and returning.
        return True if ok or False if down 
    """    
    try:
        conexion = httplib.HTTPConnection(host,port)
        conexion.request('GET',"/")
        dst_response = conexion.getresponse()
        if dst_response.status != 200:
            return False
        else:
            return True  
    except:
        return False
    
def check_couchdb_version(host,port,version):
    """
        check if the local couch db is right version
        return True if ok or False if down 
    """    
    try:
        conexion = httplib.HTTPConnection(host,port)
        conexion.request('GET',"/")
        dst_response = conexion.getresponse()
        if dst_response.status != 200:
            return False
        else:
            if dst_response.read().find('"version":"'+version+'"') != -1:
                return True
            else:
                return False  
    except:
        return False
    