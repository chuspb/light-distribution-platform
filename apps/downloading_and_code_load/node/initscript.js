/*
 Light Distribution Platform
 ===========================

 Copyright (C) 2005-2014 All rights reserved by
   Francisco José Moreno Llorca <packo@assamita.net>
   Francisco Jesús González Mata <chuspb@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var sys = require("sys"), http = require("http") , fs = require("fs");
var newmodule;
http.createServer(function (request, response) {
   
	/* INIT get init script */
   var couchdb = http.createClient(5001,"localhost");
   var request = couchdb.request("GET","/apps/_design/pldapps/message.js",{"host":"localhost"});
   request.addListener('response',function(response2){
        response2.setBodyEncoding("utf8");
        response2.addListener("data",function(chunk){                
            fd = fs.writeFileSync("./tmp/message.js",chunk,"utf8");
            newmodule = require("./tmp/message")
            sys.puts(newmodule.message());
        });
    });
    request.close()
   /* FIN  get init script */
   
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.write("OK");
  response.close();
    
}).listen(8000);
sys.puts("Server running at http://127.0.0.1:8000/");

