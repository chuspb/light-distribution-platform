/*
 Light Distribution Platform
 ===========================

 Copyright (C) 2005-2014 All rights reserved by
   Francisco José Moreno Llorca <packo@assamita.net>
   Francisco Jesús González Mata <chuspb@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var sys = require('sys'), url = require('url'), querystring = require('querystring'), path = require('path');

function create (options) {
	var module = options || {};
	
	module.initial = undefined; // initalModule
	
	module.message = function(){
		return 'tail, message: loaded';
	}
	
	module.execute = function(request, response, conf, initial) {
		var self = this;
		
		//var parsedUrl = url.parse(request.url);
		//var params = querystring.parse(parsedUrl.query.params);
		//sys.puts(params);
		
		
		var filename = path.join(process.cwd(), conf["modulesDir"] + "nodejs.log");
		sys.puts('execute, filename: ' + filename);
		
		path.exists(filename, function(exists) {
	  	if(!exists) {
	  		response.writeHead(404, {'Content-Type': 'text/plain'});
	  		response.end();
	  		return;
	  	}
	  	
	  	var tail = process.createChildProcess("tail", ["-f", filename]);
	  	
	  	
	  	response.sendHeader(200,{"Content-Type": "text/plain"});
	  	tail.addListener("output", function (data) {
	      response.write(data);
	    });
	  	tail.addListener("exit", function (code) {
	  		response.end();
	    });
		});

	}
	
	return module;
}

exports.create = create;