#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
# Light Distribution Platform
# ===========================
#
# Copyright (C) 2005-2014 All rights reserved by
#   Francisco José Moreno Llorca <packo@assamita.net>
#   Francisco Jesús González Mata <chuspb@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import sys,os,os.path,subprocess,logging,select,time,signal
sys.path.append("../libs")
import ldpcommons

def signalHandler(signum, frame):
    print("Term signal, closing NodeJS")
    global nodejs
    
    nodejs.terminate()


signal.signal(signal.SIGTERM, signalHandler)

# get config
config_path="../configurations/config.ini"
try:
    if sys.argv[1] == "-c":
        config_path = sys.argv[2]
except:
    pass
# read configurations
configs = ldpcommons.getConfig(config_path,["Nodejs","Addresses","General"])

# create working dir
log_dir = configs["general_log_dir"]
if os.path.exists(log_dir) == False:
    os.makedirs(log_dir)

# logging
logging.basicConfig(filename=os.path.join(log_dir,configs["nodejs_supervisor_log"]),level=logging.DEBUG)    


# create working dir
working_dir = os.path.join(configs["general_working_dir"],configs["nodejs_working_dir"])
if os.path.exists(working_dir) == False:
    logging.info("Creating working dir")
    os.makedirs(working_dir)
    
cache_working_dir = os.path.join(configs["general_working_dir"],configs["nodejs_working_dir"],configs["nodejs_cache_dir"])
if os.path.exists(cache_working_dir) == False:
    logging.info("Creating cache working dir")
    os.makedirs(cache_working_dir)

if ldpcommons.downloadFile(configs["ownaddress"],configs["ownport"],os.path.join("/ldp/",configs["initscript_path"],configs["initscript_file"]),os.path.join(cache_working_dir,configs["initscript_file"])) == True:
    logging.info("Downloaded initscript sucessfully")
else:
    logging.critical("ERROR: Downloaded initscript with errors")
    sys.exit(1)

while True:
    nodejs = subprocess.Popen(["node",os.path.join(cache_working_dir,configs["initscript_file"]) ],cwd=cache_working_dir,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    logging.info("Nodejs launched...")
    while nodejs.poll() == None:
        try:
            readyforread,readyforwrite,exceptready = select.select([nodejs.stdout],[],[])
            try:
                logging.info("NODEJS: "+readyforread[0].readline())
            except Exception, e:
                logging.error("exception reading nodejs state: "+str(e))
            time.sleep(1)
        except:
            pass
    nodejs.communicate()
    logging.info("Nodejs exit with exit status: "+str(nodejs.returncode))
    if nodejs.returncode == 0:
        logging.info("Nodejs exit sucessfull, exiting from supervisor.")
        sys.exit(0)
    elif nodejs.returncode == 200:
        logging.info("Nodejs notified with an update relaunching it")
        pass
    else:
        logging.critical("Nodejs finished with error status")
        sys.exit(1)



