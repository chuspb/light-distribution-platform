/*
 Light Distribution Platform
 ===========================

 Copyright (C) 2005-2014 All rights reserved by
   Francisco José Moreno Llorca <packo@assamita.net>
   Francisco Jesús González Mata <chuspb@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var sys = require("sys"),
	http = require("http"),
	fs = require("fs"),
	url = require("url"), 
	path = require("path"), 
	spawn = require('child_process').spawn,
	events = require("events");

var newmodule;
var dbHost = "127.0.0.1"; 
var dbPort = 5984; 
var modulesDir = "./"; // ./tmp/
var loadedModules = new Array(); // <"moduleName", binaryLoaded>
var loadedModulesNames = new Array(); // <"urlApp", "moduleName">
var proxiedPaths = new Array(); // <"urlApp", "proxyPath">
var nameModuleProxy = "proxy"; // name for module proxy

// Needs for httpclient to get initial configuration, like dbHost, dbPort, etc..
// TODO
var conf = new Array();
conf["dbHost"] = "127.0.0.1";
conf["dbPort"] = 5984;
conf["modulesDir"] = "./";
conf["proxiedPaths"] = new Array();


// *****************************************************************************
//
// Main Server
//
// *****************************************************************************

sys.inherits(Initial, events.EventEmitter);

function Initial(){
}

Initial.prototype.getServer = function() {
	var self = this;
	return http.createServer(self.getRequestHandler());
}

Initial.prototype.message = function() {
	var self = this;
	return "hola soy initial";
}

Initial.prototype.useModule = function(name){
	var self = this;
	return loadedModules[name];
}

Initial.prototype.requireModule = function(name, onSuccess, argsSuccess, onError, argsError){
	var self = this;
	if(undefined != loadedModules[name]){
		return loadedModules[name];
	}
	else{ // try to download the script
		self.downloadScript(name);
// self.addListener("moduleLoaded", function(nameLoaded){
// if(name == nameLoaded){
// return loadedModules[nameLoaded];
// }
// });
	}
}

Initial.prototype.getRequestHandler = function(request, response){
	var self = this;
	
	return function (request, response) {

		sys.puts("\n --- New request --- ");
		for (var j in loadedModulesNames) { // show the modules-names relation until
																				// this moment
			sys.puts('createServer, loadedModulesNames["' + j + '"]: ' + loadedModulesNames[j]);
		}
		for (var j in loadedModules) { // show the modules loaded until this moment
			sys.puts('createServer, loadedModules["' + j + '"].message(): "' + loadedModules[j].message() + '"');
		}
	
	  sys.puts("createServer, pathname: " + url.parse(request.url).pathname.split("/")[1]);
		
		var parsedUrl = url.parse(request.url);
		var appName = parsedUrl.pathname.split("/")[1]; // camaras <--
																										// http://<ip:port>/camaras
		var moduleName = loadedModulesNames[appName]; // gets the moduleName for the
																									// appName
		
		// sys.puts("createServer, appName:" + appName + ", moduleName:" +
		// moduleName);
		
		// if undefined, try to load module
		if((undefined == moduleName) || (undefined == loadedModules[moduleName])){
			
			// Get the document to check properties
			var db = http.createClient(dbPort,dbHost);
			var dbRequest = db.request("GET","/ldp/"+appName,{"host":this.dbHost});
			dbRequest.addListener('response',function(dbResponse){
				
				var data = "";
				dbResponse.addListener("data",function(chunk){
					data += chunk;
				});
				dbResponse.addListener("end", function(){
					
					sys.puts("statusCode: " + dbResponse.statusCode);
					
					if(dbResponse.statusCode == 200){
					
						if(isModule(data)){
		
							var proxyProp = getProxyProp(data);
		
							if(undefined != proxyProp){ // the app needs to be proxied
								loadedModulesNames[appName] = "proxy/proxy"; // moduleName/scriptName
								conf["proxiedPaths"][appName] = proxyProp;
								// sys.puts('createServer, conf["proxiedPaths"]["'+appName+'"]:
								// ' + conf["proxiedPaths"][appName]);
							}
							else{
								loadedModulesNames[appName] = appName+'/'+appName;
							}
							moduleName = loadedModulesNames[appName];
								
							if(undefined == loadedModules[moduleName]){ // download and load
																													// the module
								sys.puts("createServer, trying to download module: " + moduleName);
								var messageError = " module " + appName + " not loaded correctly.";
								self.downloadScript(moduleName, execute, [moduleName, request, response, conf, self], requestError, [response, messageError]); // end
																																																																								// the
																																																																								// response
							}
							else{ // 'proxy' module already loaded
								execute(moduleName, request, response, conf, self);
							}
						}				
					}
					else{
						var messageError = " error connecting with DB.";
						requestError(response, messageError);
					}
				});
			});
			
			dbRequest.end();
			
		}else{ // module previously loaded
			execute(moduleName, request, response, conf, self);
		}
	}
}

// -----------------------------------------------------------------------------
// Download and load the 'name' module
// -----------------------------------------------------------------------------
Initial.prototype.downloadScript = function(name, onSuccess, argsSuccess, onError, argsError){
	var self = this;
	
	var splitName = name.split("/");
	var moduleName = splitName[0];
	var scriptName = splitName[1];
	
	var db = http.createClient(dbPort,dbHost);
	var dbRequest = db.request("GET","/ldp/"+name+".js",{"host":dbHost});
	dbRequest.end();
	dbRequest.addListener('response',function(dbResponse){
		var data = "";
		dbResponse.setBodyEncoding("utf8");
		dbResponse.addListener("data",function(chunk){
			data += chunk;
		});
		dbResponse.addListener("end",function(){
			if(dbResponse.statusCode == 200){
	     path.exists(modulesDir+moduleName, function(exists) {
	    	 sys.debug(exists ? 'its there '+ modulesDir+moduleName + ', ' + exists : 'no passwd!');
		    	if(!exists) {
		    		var mkdir = spawn("mkdir", ["", moduleName]);
			    	mkdir.addListener("exit", function (code) {
			    		code = code%256;
			    		sys.puts('downloadScript, mkdir ' + moduleName + ' ,exitCode: ' + code);
			    		if(code == 0){
			          fd = fs.writeFileSync(modulesDir+name+".js",data,"utf8");
			          module.moduleCache = {};
			          // loadedModules[name] = require(modulesDir+name);
			          // loadedModules[name].setInitial(this);
			          // sys.puts(loadedModules[name].setInitial(this));
			          var kk = require(modulesDir+name);
			          loadedModules[name] = kk.create();
			          sys.puts(loadedModules[name].message());
			          self.emit("moduleLoaded", name);
			          if(undefined != onSuccess){
			          	onSuccess.apply(null, argsSuccess);
			        	}
			    		}
			      });
		    	}else{
		    		fd = fs.writeFileSync(modulesDir+name+".js",data,"utf8");
		    		module.moduleCache = {};
						// loadedModules[name] = require(modulesDir+name);
						// loadedModules[name].setInitial(this);
						// sys.puts(loadedModules[name].setInitial(this));
		    		var kk = require(modulesDir+name);
						loadedModules[name] = kk.create();
						sys.puts(loadedModules[name].message());
						self.emit("moduleLoaded", name);
						if(undefined != onSuccess){
							onSuccess.apply(null, argsSuccess);
						}
		    	}
		    });
			}
			else{
				if(undefined != onError){
					onError.apply(null, argsError);
	   	}
			}
	 });
	});
}

// *****************************************************************************
//
// Server to update modules
// receives -> {"update":["moduleName1","moduleName2","moduleName3"]}
//
// *****************************************************************************

Initial.prototype.getServerUpdates = function() {
	var self = this;
	return http.createServer(self.getRequestHandlerUpdates());
}

Initial.prototype.getRequestHandlerUpdates = function(request, response){
	var self = this;
	
	return function (request, response) {
		var data = "";
		request.addListener("data", function (chunk) {
			data += chunk;
		});
		
		request.addListener("end", function () {
			var parsedUrl = url.parse(request.url);
			
			if(validateRequest(request, data)){

				var updatedNames = json2object(data).updated;
				if(undefined !== updatedNames){
					for(var i in updatedNames){
						self.updateModule(updatedNames[i], response); // Update the
																													// <module>.js
					}
				}
				var deletedNames = json2object(data).deleted;
				if(undefined !== deletedNames){
					for(var i in deletedNames){
						deleteModule(deletedNames[i], response); // Delete the <module>.js
					}
				}
				var createdNames = json2object(data).created;
				if(undefined !== createdNames){
					for(var i in createdNames){
						self.createModule(createdNames[i], response); // Get the <module>.js
					}
				}
			}
			else{ // not correct path
				response.writeHead(403, {'Content-Type': 'text/plain'});
				response.end();
			}
	  });
	}
}

// -----------------------------------------------------------------------------
// Updates a module.
// -----------------------------------------------------------------------------
Initial.prototype.updateModule = function(name, response){
	
	var self = this;
	
	if(name == "initialmodule"){
		process.exit(200);
	}
	
	if(undefined !== loadedModules[name+"/"+name]){ // if the module was already
																									// loaded
		
		sys.puts("updateModule, " + name);
		try{
			// Get the doc and check if is a module
			var db = http.createClient(dbPort,dbHost);
			var dbRequest = db.request("GET","/ldp/"+name,{"host":this.dbHost});
			dbRequest.addListener('response',function(dbResponse){
				var data = "";
				dbResponse.addListener("data",function(chunk){
					data += chunk;
				});
				dbResponse.addListener("end", function(){
					sys.puts("statusCode: " + dbResponse.statusCode);
					if(dbResponse.statusCode == 200){
						if(isModule(data) && (undefined==getProxyProp(data))){
							sys.puts('updateModule, module: "' + name + '" is not proxied, so let\'s go for it');
							
							for(var i in loadedModules){
								var moduleName = i.split("/")[0]; 
								var scriptName = i.split("/")[1]; 
								if(name == moduleName){ // we must update each loaded script of
																				// this module
									var messageSuccess = " module " + moduleName + " was updated correctly";
									var messageError = " module " + moduleName + " was not updated correctly";
									self.downloadScript(i, requestSuccess, [response, messageSuccess ], requestError, [response, messageError]);
								}
							}
						}
					}
				});
			});
			dbRequest.end();
		}catch(e){
			sys.puts("updateModule, error with script: " + name + ". Description: " + e.description);
		}
	}
	else{
		requestError(response, " module " + name + " was not loaded previously." );
	}
}

// -----------------------------------------------------------------------------
// Creates a module.
// -----------------------------------------------------------------------------
Initial.prototype.createModule = function(name, response){
	
	var self = this;
	var moduleName = name;
	var scriptName = name;
	
	try{
		// Get the doc and check if is a module
		var db = http.createClient(dbPort,dbHost);
		var dbRequest = db.request("GET","/ldp/"+moduleName,{"host":this.dbHost});
		dbRequest.addListener('response',function(dbResponse){
			var data = "";
			dbResponse.addListener("data",function(chunk){
				data += chunk;
			});
			dbResponse.addListener("end",function(){
				if(isModule(data) && undefined==getProxyProp(data)){
					sys.puts('createModule, module: "' + name + '" is not proxied, so let\'s go for it');
					var messageSuccess = " module " + moduleName + " was updated correctly";
					var messageError = " module " + moduleName + " was not updated correctly";
					// maybe call execute or other function
					self.downloadScript(moduleName+"/"+scriptName, requestSuccess, [response, messageSuccess ], requestError, [response, messageError]);
				}
			});
		});
		dbRequest.end();
	}catch(e){
		sys.puts("createModule, error with script: " + moduleName+"/"+scriptName + ". Description: " + e.description);
	}
}

// -----------------------------------------------------------------------------
// Loads the autorun modules.
// -----------------------------------------------------------------------------
Initial.prototype.loadAutorunModules = function(){
	var self = this;
	var view = "_design/module_tools/_view/autorun_modules";
	
	try{
		// Get the view and check if is a module and is autorun
		var db = http.createClient(dbPort,dbHost);
		var dbRequest = db.request("GET","/ldp/"+view,{"host":this.dbHost});
		dbRequest.addListener('response',function(dbResponse){
			var data = "";
			dbResponse.addListener("data",function(chunk){
				data += chunk;
			});
			dbResponse.addListener("end",function(){
				
				var rows = json2object(data).rows;
				
				for(var i in rows){
					sys.puts('loadAutorunModules, module: "' + rows[i].id + '"');
					var moduleName = rows[i].id;
					var scriptName = rows[i].id;
					var messageError = " module " + moduleName + " was not updated correctly";
					// maybe call execute or other function
					self.downloadScript(moduleName+"/"+scriptName, execute, [moduleName+"/"+scriptName, null, null, conf, self], puts, [messageError]);
				}
			});
		});
		dbRequest.end();
	}catch(e){
		sys.puts("loadAutorunModules, error with script: " + moduleName+"/"+scriptName + ". Description: " + e.description);
	}
}

exports.Initial = Initial;


// *****************************************************************************
//
// Internal functions
//
// *****************************************************************************


// -----------------------------------------------------------------------------
// Check if 'data' has the property type=module
// -----------------------------------------------------------------------------
function isModule(data){
	
	if ("module" == json2object(data).type) {
		return true;
	} else {
		return false;
	}
}

// -----------------------------------------------------------------------------
// Check if 'data' has the property proxy!=''
// -----------------------------------------------------------------------------
function getProxyProp(data){
	if (undefined != json2object(data).proxy && "" != json2object(data).proxy) {	
		return json2object(data).proxy;
	} else {
		return undefined;
	}
}

// -----------------------------------------------------------------------------
// Check if 'data' has the property autorun=true
// -----------------------------------------------------------------------------
function isAutorun(data){

	if ("true" == json2object(data).autorun) {
		return true;
	} else {
		return false;
	}
}

// -----------------------------------------------------------------------------
// Execute the method 'execute' for the module 'moduleName'
// -----------------------------------------------------------------------------
function execute(moduleName, request, response, conf, initial){
	loadedModules[moduleName].execute(request, response, conf, initial); // end
																																				// the
																																				// response
}

// -----------------------------------------------------------------------------
// Prepare the response to show the error message
// -----------------------------------------------------------------------------
function requestError(response, message){
	response.writeHead(500, {'Content-Type': 'text/plain'});
	response.write("Server, " + message + "\n");
	sys.puts("Server, " + message);
	response.end();
}

// -----------------------------------------------------------------------------
// Prepare the response to show the success message
// -----------------------------------------------------------------------------
function requestSuccess(response, message){
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.write("Server, " + message + "\n");
	sys.puts("Server, " + message);
	response.end();
}

// -----------------------------------------------------------------------------
// Print a message
// -----------------------------------------------------------------------------
function puts(message){
	sys.puts(message);
}

// -----------------------------------------------------------------------------
// Parse a json string into a javascript object
// -----------------------------------------------------------------------------
function json2object(json){
	return eval('(' + json + ')'); 	// return JSON.parse(json);
}

// -----------------------------------------------------------------------------
// Check if the request data is correct
// supported schemas -> {"updated":["moduleName1","moduleName2","moduleName3"]}
// {"deleted":["moduleName1","moduleName2","moduleName3"]}
// {"created":["moduleName1","moduleName2","moduleName3"]}
// -----------------------------------------------------------------------------
function validateRequest(request, data){
	var parsedUrl = url.parse(request.url);
	var validation = false;
	
	try{
		if("/updates" == parsedUrl.pathname){
			var updated = json2object(data).updated;
			var deleted = json2object(data).deleted;
			var created = json2object(data).created;
			
			if(undefined !== updated && updated instanceof Array){
				for(var i in updated){
					validation = true;
					if(typeof(updated[i])!="string"){
						validation = false;
					}
				}
			}
			if(undefined !== deleted && deleted instanceof Array){
				for(var i in deleted){
					validation = true;
					if(typeof(deleted[i])!="string"){
						validation = false;
					}
				}
			}
			if(undefined !== created && created instanceof Array){
				for(var i in created){
					validation = true;
					if(typeof(created[i])!="string"){
						validation = false;
					}
				}
			}
		}
	}catch(e){
		sys.puts("validateRequest, data not validated: " + data);
	}
	return validation;
}

// -----------------------------------------------------------------------------
// Try to delete a module
// -----------------------------------------------------------------------------
function deleteModule(name, response){
	var exist = false;
	for(var i in loadedModules){
		var moduleName = i.split("/")[0];
		var scriptName = i.split("/")[1];
		if(name == moduleName){ // we must delete each loaded script of this module
			exist = true;
			var messageSuccess = " module " + moduleName + " was deleted correctly";
			var messageError = " module " + moduleName + " was not deleted correctly";
			deleteScript(i, requestSuccess, [response, messageSuccess], requestError, [response, messageError]);
		}
	}
	if(!exist){
		requestError(response, " module " + name + " was not loaded previously." );
	}
}


// -----------------------------------------------------------------------------
// Try to delete a module
// -----------------------------------------------------------------------------
function deleteScript(name, onSuccess, argsSuccess, onError, argsError){
	
	for(var i in loadedModules){
		sys.puts("deleteScripts, loadedModules: " + i);
	}

	if(undefined !== loadedModules[name]){ // if the module was already loaded
		
		try{
			
			module.moduleCache = {};
			
			// Update the structures
			delete loadedModules[name];
			for(var i in loadedModulesNames){
				if(name === loadedModulesNames[i]){
					delete loadedModulesNames[i];
				}
			}
			
			// Try to delete the binary
			var filename = path.join(process.cwd(), modulesDir+name+".js");
	    path.exists(filename, function(exists) {
	    	if(!exists) {
	    		return;
	    	}
	    	
	    	sys.puts('deleteScript, deleting: ' + filename);
	    	
	    	var rm = spawn("rm", ["-rf", filename]);
	    	
	    	rm.addListener("exit", function (code) {
	        sys.puts('deleteScript, deleting ' + name+'.js' + ' ,exitCode: ' + code);
	        if(undefined != onSuccess){
	        	onSuccess.apply(null, argsSuccess);
	      	}
	      });
	    	
	    	rm.addListener('uncaughtException', function (err) {
	    	  sys.puts('deleteScript, Caught exception: ' + err);
	    	  if(undefined != onError){
						onError.apply(null, argsError);
	      	}
	    	});
	    	
	    });
			
		}catch(e){
			sys.puts("deleteScript, error with script: " + name + ". Description: " + e.description);
			if(undefined != onError){
				onError.apply(null, argsError);
	  	}
		}
	}
}


var s = new Initial();

var server = s.getServer();
server.listen(8000);
sys.puts("Server running at http://127.0.0.1:8000/");

var serverUpdates = s.getServerUpdates();
serverUpdates.listen(8001);
sys.puts("ServerUpdates running at http://127.0.0.1:8001/");

s.loadAutorunModules();
sys.puts("Server loading autorun modules");


