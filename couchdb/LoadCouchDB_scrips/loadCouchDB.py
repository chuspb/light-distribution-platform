#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
# Light Distribution Platform
# ===========================
#
# Copyright (C) 2005-2014 All rights reserved by
#   Francisco José Moreno Llorca <packo@assamita.net>
#   Francisco Jesús González Mata <chuspb@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import ConfigParser, subprocess , time, signal,sys,logging,os

sys.path.append("../../libs")
import ldpcommons
import couchdb

def signalHandler(signum, frame):
    print("Term signal, closing CouchDB")
    global couchdb_instance
    
    couchdb_instance.terminate()
    
def launchCouchDB(bin_path,default,local):
    """
        launch a couchDB instance and return the subprocess object
    """
    return subprocess.Popen([bin_path,"-a",default,"-a",local,"-p","/tmp/couchdb.pid"])

    
def replicateDatabases(masteraddress,masterport,ownaddress,ownport,database):
    """
        launch the replicate commands if is not a master
    """
    
    # check if the database to replicate exists
    time.sleep(2)
    couchs = couchdb.Server("http://"+ownaddress+":"+ownport+"/")
    try: 
        couchs[database]
        logging.info("Database found, replicating")
    except :
        logging.info("Database not found, creating...") 
        couchs.create(database)
        time.sleep(3)
    #in next release
    #couchs.get_or_create_db(database)
    
    time.sleep(1)
    logging.info("Replicating in one way")
    couchs = couchdb.Server("http://localhost:"+ownport+"/")
    time.sleep(5)
    logging.info("Replicating in other way")
    couchs.replicate(source=database,target='http://'+masteraddress+':'+masterport+'/'+database,continuous=True)
    time.sleep(5)
    couchs.replicate(source='http://'+masteraddress+':'+masterport+'/'+database,target=database,continuous=True)

def createCouchDBConfig(configs):
    parser = ConfigParser.ConfigParser()
    try:
        parser.readfp(open(configs["local"]))
        parser.set("couchdb")
        # will continue
    except Exception,e:
        logging.error("Error reading local.ini file: "+str(e))
    
    

# get config
config_path="../../configurations/config.ini"
try:
    if sys.argv[1] == "-c":
        config_path = sys.argv[2]
except:
    pass
    
configs = ldpcommons.getConfig(config_path,["CouchDB","Addresses","General"])

# create working dir
couchdb_data_dir = configs["couchdb_working_dir"]
if os.path.exists(couchdb_data_dir) == False:
    os.makedirs(couchdb_data_dir)

# create log dir
log_dir = configs["general_log_dir"]
if os.path.exists(log_dir) == False:
    os.makedirs(log_dir)

# logging
logging.basicConfig(filename=os.path.join(log_dir,configs["couchdb_supervisor_log"]),level=logging.DEBUG)    

# run couchdb
couchdb_instance = launchCouchDB(configs["bin_couchdb"],configs["default"],configs["local"]) 
logging.info("Launch with pid: "+str(couchdb_instance.pid))
signal.signal(signal.SIGTERM, signalHandler)

# check couchdb avaliability
couchdb_ok = False
tries = 0
while couchdb_ok != True and tries < 5:
    time.sleep(2)
    couchdb_ok = ldpcommons.check_couchdb_up("localhost",configs["ownport"])
    # increment counter
    tries = tries+1
if couchdb_ok == False:
    logging.error("Couchdb is not responding, exiting...")
    couchdb_instance.terminate()
    time.sleep(2)
    if couchdb_instance.poll != None:
        couchdb_instance.kill()
    sys.exit(1)

if configs["master"] == "no":
    replicateDatabases(configs["masteraddress"],configs["masterport"],configs["ownaddress"],configs["ownport"],configs["database"])

couchdb_instance.communicate()
state = couchdb_instance.returncode
if state != 0:
    logging.info("CouchDB has terminated with error, state: "+str(state))
else:
    logging.info("Exit with sucess")
        
    
    

    
    
