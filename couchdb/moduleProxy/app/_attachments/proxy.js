/*
 Light Distribution Platform
 ===========================

 Copyright (C) 2005-2014 All rights reserved by
   Francisco José Moreno Llorca <packo@assamita.net>
   Francisco Jesús González Mata <chuspb@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

var sys = require('sys'), url = require('url'), http = require('http');

binaryContentTypes = ['application/octet-stream', 'application/ogg', 'application/zip', 'application/pdf',
                      'image/gif', 'image/jpeg', 'image/png', 'image/tiff', 'image/vnd.microsoft.icon',
                      'multipart/encrypted', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint',
                      'application/msword', 'application/x-dvi', 'application/x-shockwave-flash', 
                      'application/x-stuffit', 'application/x-rar-compressed', 'application/x-tar']

var guessEncoding = function (contentEncoding, contentType, accept) {

  var encoding = "iso-8859-1"; 
  if (contentEncoding === undefined && contentType === undefined && accept === undefined) {
    return "binary"
  }
  if (contentEncoding == 'gzip') {
    encoding = "binary";
  } 
	else{ 
		//setting contentType with 'accept' to prevent errors
		if(contentType === undefined && accept !== undefined){ 
			if(accept.indexOf(',') !== -1){			
				contentType = accept.split(',')[0];
			}
		}

		if (contentType) {
		  if (contentType.indexOf('charset=') !== -1) {
		    encoding = contentType.split('charset=')[1];
		    if (encoding.toLowerCase() === 'utf-8') { return 'utf8'; }
		    if (encoding.toLowerCase() === 'ascii') { return 'ascii'; }
		    return "binary"
		  } else if (contentType.slice(0,6) == 'video/' || contentType.slice(0,6) == 'audio/') {
		    encoding = "binary";
		  } else if (binaryContentTypes.indexOf(contentType) != -1) {
		    encoding = "binary";
		  }
		}
	}

  return encoding;
}

function create (options) {
	
	var module = options || {};
	
	module.message = function(){
		return 'moduleProxy: message: loaded';
	}
	
	module.execute = function(clientRequest, clientResponse, conf) {
		
		// Url transformation
		var pathname =  url.parse(clientRequest.url).pathname;
		var search = url.parse(clientRequest.url).search;
		var hash = url.parse(clientRequest.url).hash;
		
	  var appPath = pathname.split("/")[1];
	  var appTargetPath = conf["proxiedPaths"][appPath];
	  
	  var newPathName = "/ldp/" + appTargetPath; // /ldp/camcom3

	  // newPathName
	  var index = pathname.substr(1).indexOf("/");
	  if(index != -1){ // there is subpathname
	  	newPathName += pathname.substr(index+1);
	  }
	  if(undefined != search){
	  	newPathName += search;
	  }
	  if(undefined != hash){
	  	newPathName += hash;
	  }

	  sys.puts('execute, newPathName:' + newPathName);
	  
		var c = http.createClient(conf["dbPort"], conf["dbHost"]);
		
		var clientError = function (error) {
	    clientResponse.writeHead(504, "504 Gateway Timeout", {
	      "content-type":"text/plain",
	      "content-length":"504 Gateway Timeout".length,
	    })
	    clientResponse.write("504 Gateway Timeout", "ascii");
	    clientResponse.end();
	    c.removeListener("error", clientError);
	  }
	  c.addListener("error", clientError);
		
		var proxyRequest = c.request("GET", newPathName, clientRequest.headers);
		
		//respuesta de datos desde el proxy al cliente
		proxyRequest.addListener('response', function (response) {
			response.csize = 0;
			clientResponse.writeHead(response.statusCode, response.headers);
			response.addListener("data", function (chunk) {
				response.csize += chunk.length;
				clientResponse.write(chunk);
			});
			response.addListener("end", function () {
	      clientResponse.end();
	      c.removeListener("error", clientError);
	      c.busy = false;
	    });
			// work-around for end event bug in node
	    if (clientRequest.method == 'HEAD') {
	      response.emit("end");
	    }
		});
		
		//envio de request desde el proxy
	  clientRequest.addListener("data", function (chunk) {
	    proxyRequest.write(chunk);
	  })
	  clientRequest.addListener("end", function () {
	    proxyRequest.end();
	  })
	  
	}
	
	return module;
}

exports.create = create;
