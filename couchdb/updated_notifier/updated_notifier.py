#!/usr/bin/env python
# -*- coding: UTF8 -*-
#
# Light Distribution Platform
# ===========================
#
# Copyright (C) 2005-2014 All rights reserved by
#   Francisco José Moreno Llorca <packo@assamita.net>
#   Francisco Jesús González Mata <chuspb@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os, sys, signal, ConfigParser, logging
import simplejson as json
import couchdbhttp as chttp

sys.path.append("../../libs")

import couchdb

map_fun1 = u'''{"map":"function(doc) { if (doc.type == 'module' && doc._id == \''''
map_fun2 = u'''\' ) emit(doc._id, null); }"}'''
     


def getConfig(conf_path,sections):
    """
        read configurations from a config.ini
        with a section list
        and return as dictionary
    """
    
    return_dict = {}
    parser = ConfigParser.ConfigParser()
    try:
        parser.readfp(open(conf_path))
    except:
        print("Leyendo archivo de configuraciones")
    
    try:
        for section in sections:
            for param in parser.items(section): 
                try:
                    return_dict[param[0]] = param[1]
                except:
                    print("Error getting: "+section+":"+param)
    except Exception,e:
        print("Error getting information from conf.ini: "+ str(e))
        
    return return_dict

def signalHandler(signum, frame):
    print("Term signal, closing updated_notifier")
    sys.exit(0)
    
def notify_apps_updated(ip,port,url,doc,operation):
    http = chttp.Couch(ip,port)
    body=u'{"'+operation+'" : ["'+doc+'"]}'
    response = http.post(url,body)
    logging.debug(str(response.status))
    logging.debug(response.reason)
    if response.status == 200:
        return True
    else:
        return False
    

signal.signal(signal.SIGTERM, signalHandler)

# get config
configs = getConfig("../../configurations/config.ini",["updated_notifier","Addresses","General"])

# create working dir
log_dir = configs["general_log_dir"]
if os.path.exists(log_dir) == False:
    os.makedirs(log_dir)

# logging
logging.basicConfig(filename=os.path.join(log_dir,configs["updated_logfile"]),level=logging.DEBUG)    


# get the base update_seq
couchserver = couchdb.Server("http://"+str(configs["ownaddress"])+":"+str(configs["ownport"]))
couchdatabase = couchserver[configs["database"]]
update_seq = couchdatabase.info()["update_seq"]
logging.debug("Initial updateseq: "+str(update_seq))

while True:
    # parse update notification
    update = sys.stdin.readline()
    logging.debug("New message: "+update)
    http = chttp.Couch(configs["ownaddress"],configs["ownport"])
     
    try:
        parsed = json.loads(update)
        if parsed["db"] == configs["database"]:
            logging.debug("Database right")
            operation = parsed["type"]
            logging.debug("Operation: "+operation)
            try:
                new_update_seq = couchdatabase.info()["update_seq"]
                logging.debug("New updateseq: "+str(update_seq))
            except Exception,e:
                logging.error("ERROR connecting to server: "+str(e))
                continue
            if update_seq == 0:
                previous = 0
            else:
                previous = update_seq-1
            try:
                logging.info("get updated command: "+'/'+configs["database"]+'/_changes?since='+str(previous)+'&include_docs=true')
                results = json.loads(http.get('/'+configs["database"]+'/_changes?since='+str(previous)+'&include_docs=true').read())["results"]
                for result in results:
                    doc = result["doc"]
                    if doc.has_key("type"):
                        if doc["type"] == "module":
                            logging.debug("Module doc, updating")
                            if notify_apps_updated(configs["notificationip"],configs["notificationport"],configs["notificationurl"],doc["_id"],operation) == True:
                                logging.debug("Notified successfull")
                                update_seq = result["seq"]
                                logging.debug("New update seq: "+str(update_seq))
                            else:
                                logging.error("Error notifing")
                logging.debug("Updated: "+doc["_id"])
            except Exception,e:
                logging.error("Error getting last update: "+str(e))
                continue
            
    except Exception,e:
        logging.debug(str(e))
        sys.exit(0)

